package profe.ms.empleadosweb.exceptions;

public class EmpleadoNotFoundException extends EmpleadosException {

	public EmpleadoNotFoundException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNotFoundException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNotFoundException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNotFoundException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
